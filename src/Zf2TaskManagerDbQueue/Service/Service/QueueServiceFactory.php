<?php
namespace Zf2TaskManagerDbQueue\Service\Service;

use Zf2TaskManagerDbQueue\Service\QueueService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class QueueServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $mapper = $serviceLocator->get('Zf2TaskManagerDbQueue\Mapper\QueueMapper');
        return new QueueService($mapper);
    }
}