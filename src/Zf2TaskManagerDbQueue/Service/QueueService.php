<?php

namespace Zf2TaskManagerDbQueue\Service;

use Zf2TaskManagerDbQueue\Entity\QueueItem;
use Zend\Stdlib\Hydrator\ClassMethods;

class QueueService extends AbstractService
{
    /** @var \Zf2TaskManagerDbQueue\Mapper\QueueMapper */
    protected $mapper;

    /**
     * @param int $id
     * @return QueueItem
     */
    public function get($id)
    {
        $item = $this->mapper->getEntityByFields(array(
            'queue_item_id' => $id,
        ));
        return $item;
    }

    /**
     * @param QueueItem $queue
     */
    public function insert(QueueItem $queue)
    {
        $lastInsertedValue = $this->mapper->insert($queue);
        $new = $this->mapper->getEntityByFields(array(
            'queue_item_id' => $lastInsertedValue,
        ));
        $classMethod = new ClassMethods();
        $data = $classMethod->extract($new);
        $classMethod->hydrate($data, $queue);
    }
    /**
     * @return QueueItem|null
     */
    public function getJob()
    {
        return $this->mapper->getJob();
    }


    /**
     * @param QueueItem $queue
     */
    public function fail(QueueItem $queue)
    {
        $this->mapper->fail($queue);
    }

    /**
     * @param QueueItem $queue
     */
    public function lock(QueueItem $queue)
    {
        $this->mapper->lock($queue);
    }

    /**
     * @param QueueItem $queue
     */
    public function complete(QueueItem $queue)
    {
        $this->mapper->complete($queue);
    }

    /**
     * @param QueueItem $queue
     */
    public function delete(QueueItem $queue)
    {
        $this->mapper->delete($queue);
    }
}