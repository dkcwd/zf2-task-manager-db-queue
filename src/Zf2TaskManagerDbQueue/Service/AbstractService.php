<?php

namespace Zf2TaskManagerDbQueue\Service;

use Zf2TaskManagerDbQueue\Mapper\AbstractMapper;

abstract class AbstractService
{
    /** @var AbstractMapper */
    protected $mapper;

    public function __construct(AbstractMapper $mapper)
    {
        $this->mapper = $mapper;
    }
}