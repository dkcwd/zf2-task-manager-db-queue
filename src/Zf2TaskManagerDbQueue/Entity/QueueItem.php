<?php

namespace Zf2TaskManagerDbQueue\Entity;

use DateTime;

class QueueItem
{
    const STATUS_READY_FOR_PROCESSING   = 'Ready';
    const STATUS_LOCKED_FOR_PROCESSING  = 'Locked';
    const STATUS_PROCESSING_COMPLETE    = 'Finished';
    const STATUS_ERROR_WHILE_PROCESSING = 'Error';

    const EXTREME_HIGH_PRIORITY = 200;
    const HIGH_PRIORITY         = 100;
    const NORMAL_PRIORITY       = 50;
    const LOW_PRIORITY          = 1;

    const RETRY_COUNT           = 3;

    /** @var int */
    protected $queueItemId;

    /** @var string */
    protected $queueItemStatus;

    /** @var string */
    protected $queueItemMessage;

    /** @var DateTime */
    protected $queueItemStartDateTime;

    /** @var DateTime */
    protected $queueItemCreatedDate;

    /** @var int */
    protected $queueItemPriorityOrder;

    /**
     * @param int $queueItemId
     */
    public function setQueueItemId($queueItemId)
    {
        $this->queueItemId = $queueItemId;
    }

    /**
     * @return int
     */
    public function getQueueItemId()
    {
        return $this->queueItemId;
    }

    /**
     * @param string $queueItemStatus
     */
    public function setQueueItemStatus($queueItemStatus)
    {
        $this->queueItemStatus = $queueItemStatus;
    }

    /**
     * @return string
     */
    public function getQueueItemStatus()
    {
        return $this->queueItemStatus;
    }

    /**
     * @param string $queueItemMessage
     */
    public function setQueueItemMessage($queueItemMessage)
    {
        $this->queueItemMessage = $queueItemMessage;
    }

    /**
     * @return string
     */
    public function getQueueItemMessage()
    {
        return $this->queueItemMessage;
    }

    /**
     * @param \DateTime $queueItemStartDateTime
     */
    public function setQueueItemStartDateTime($queueItemStartDateTime)
    {
        $this->queueItemStartDateTime = $queueItemStartDateTime;
    }

    /**
     * @return \DateTime
     */
    public function getQueueItemStartDateTime()
    {
        return $this->queueItemStartDateTime;
    }

    /**
     * @param \DateTime $queueItemCreatedDate
     */
    public function setQueueItemCreatedDate($queueItemCreatedDate)
    {
        $this->queueItemCreatedDate = $queueItemCreatedDate;
    }

    /**
     * @return \DateTime
     */
    public function getQueueItemCreatedDate()
    {
        return $this->queueItemCreatedDate;
    }

    /**
     * @param int $queueItemPriorityOrder
     */
    public function setQueueItemPriorityOrder($queueItemPriorityOrder)
    {
        $this->queueItemPriorityOrder = $queueItemPriorityOrder;
    }

    /**
     * @return int
     */
    public function getQueueItemPriorityOrder()
    {
        return $this->queueItemPriorityOrder;
    }

}
