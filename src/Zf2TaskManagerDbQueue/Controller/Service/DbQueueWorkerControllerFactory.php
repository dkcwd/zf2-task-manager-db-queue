<?php

namespace Zf2TaskManagerDbQueue\Controller\Service;

use Zf2TaskManagerDbQueue\Controller\DbQueueWorkerController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * WorkerFactory
 */
class DbQueueWorkerControllerFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $worker = $serviceLocator->getServiceLocator()->get('Zf2TaskManagerDbQueue\Worker\DbQueueWorker');
        return new DbQueueWorkerController($worker);
    }
}