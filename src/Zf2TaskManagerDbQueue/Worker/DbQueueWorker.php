<?php

namespace Zf2TaskManagerDbQueue\Worker;

use Exception;
use SlmQueue\Job\JobInterface;
use SlmQueue\Queue\QueueInterface;
use SlmQueue\Worker\AbstractWorker;
use SlmQueue\Worker\WorkerEvent;

class DbQueueWorker extends AbstractWorker
{
    /**
     * {@inheritDoc}
     */
    public function processQueue($queueName, array $options = array())
    {
        if (ZF2_TASK_MANAGER_DB_QUEUE_USE_ALTERNATE_ERROR_LOG) {
            /**
             * Report any fatal errors, the process won't be stopped,
             * but a specific log file will be monitored
             */
            register_shutdown_function(function () {
                $errfile = "unknown file";
                $errstr  = "shutdown";
                $errno   = E_CORE_ERROR;
                $errline = 0;

                $error = error_get_last();

                if( $error !== NULL) {
                    $errno   = $error["type"];
                    $errfile = $error["file"];
                    $errline = $error["line"];
                    $errstr  = $error["message"];

                    file_put_contents(
                        ZF2_TASK_MANAGER_DB_QUEUE_ERROR_LOG,
                        implode(' ', array(date('d-m-Y H:i:s') . ':', 'Error: ' . $errstr, 'File: ' . $errfile, 'Line: ' . $errline)) . PHP_EOL,
                        FILE_APPEND
                    );

                }
            });
        }

        /** @var $queue QueueInterface */
        $queue        = $this->queuePluginManager->get($queueName);
        $eventManager = $this->getEventManager();
        $count        = 0;

        $workerEvent = new WorkerEvent($queue);
        $eventManager->trigger(WorkerEvent::EVENT_PROCESS_QUEUE_PRE, $workerEvent);

        while (true) {
            // Check for external stop condition
            if ($this->isStopped()) {
                break;
            }

            $job = $queue->pop($options);

            // The queue may return null, for instance if a timeout was set
            if (!$job instanceof JobInterface) {
                // Check for internal stop condition
                if ($this->isMaxMemoryExceeded()) {
                    // If no job available to process wait before trying again
                    echo date('d-m-Y H:i:s') . ': memory limit exceeded' . PHP_EOL;
                    sleep(15);
                    break;
                }

                // If no job available to process wait before trying again
                echo date('d-m-Y H:i:s') . ': no jobs found....waiting' . PHP_EOL;
                sleep(15);
                continue;
            }

            $workerEvent->setJob($job);

            $eventManager->trigger(WorkerEvent::EVENT_PROCESS_JOB_PRE, $workerEvent);

            // Job ready for processing
            echo date('d-m-Y H:i:s') . ': job found....processing' . PHP_EOL;

            /**
             * We want the process to continue running in case of simple time outs
             */
            try {
                $this->processJob($job, $queue);
                $count++;
            } catch (\Exception $e) {
                // An error occurred while processing
                echo date('d-m-Y H:i:s') . ': An error occurred while processing, the message is: ' . $e->getMessage() . PHP_EOL;
                // Log exception
                if (ZF2_TASK_MANAGER_DB_QUEUE_USE_ALTERNATE_ERROR_LOG) {
                    file_put_contents(
                        ZF2_TASK_MANAGER_DB_QUEUE_ERROR_LOG,
                        implode(' ', array(date('d-m-Y H:i:s') . ':', 'Error: ' . $e->getMessage(), 'File: ' . $e->getFile(), 'Line: ' . $e->getLine())) . PHP_EOL,
                        FILE_APPEND
                    );
                }
                sleep(5);
                continue;
            }

            $eventManager->trigger(WorkerEvent::EVENT_PROCESS_JOB_POST, $workerEvent);

            // Check for internal stop condition
            if ($this->isMaxRunsReached($count) || $this->isMaxMemoryExceeded()) {
                break;
            }
        }

        $eventManager->trigger(WorkerEvent::EVENT_PROCESS_QUEUE_POST, $workerEvent);

        return $count;
    }

    /**
     * {@inheritDoc}
     */
    public function processJob(JobInterface $job, QueueInterface $queue)
    {
        try {
            $queue->lock($job);
            $job->execute();
        } catch (Exception $exception) {
            $queue->fail($job);
            throw $exception;
        }

        $queue->complete($job);
        sleep(1);
    }
}
