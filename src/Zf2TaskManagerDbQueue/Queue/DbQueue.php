<?php

namespace Zf2TaskManagerDbQueue\Queue;

use Zf2TaskManagerDbQueue\Entity\QueueItem;
use SlmQueue\Job\JobInterface;
use SlmQueue\Job\JobPluginManager;
use SlmQueue\Queue\AbstractQueue;

/**
 * DbQueue
 */
class DbQueue extends AbstractQueue
{
    protected $queue;

    /**
     * Constructor
     *
     * @param mixed            $queue
     * @param string           $name
     * @param JobPluginManager $jobPluginManager
     */
    public function __construct($queue, $name, JobPluginManager $jobPluginManager)
    {
        $this->queue = $queue;
        parent::__construct($name, $jobPluginManager);
    }

    /**
     * Valid option is:
     *      - delay_seconds: the duration (in seconds) the message has to be delayed
     *
     * {@inheritDoc}
     */
    public function push(JobInterface $job, array $options = array())
    {
        $queueItem = new QueueItem();
        $queueItem->setQueueItemStatus(QueueItem::STATUS_READY_FOR_PROCESSING);
        $queueItem->setQueueItemMessage($job->jsonSerialize());
        $queueItem->setQueueItemStartDateTime(new \DateTime());
        $queueItem->setQueueItemCreatedDate(new \DateTime());
        $queueItem->setQueueItemPriorityOrder(QueueItem::NORMAL_PRIORITY);
        // insert job in the queue
        $this->queue->insert($queueItem);
    }

    /**
     * {@inheritDoc}
     */
    public function pop(array $options = array())
    {
        /** @var QueueItem $queueItem */
        $queueItem = $this->queue->getJob();
        if (empty($queueItem)) return null;
        $message = json_decode($queueItem->getQueueItemMessage());
        $job = $this->getJobPluginManager()->get($message->class);
        $job->setContent(unserialize($message->content));
        $job->setMetadata(array('id' => $queueItem->getQueueItemId()));

        return $job;
    }

    /**
     * {@inheritDoc}
     */
    public function delete(JobInterface $job)
    {
        $queueItem = new QueueItem();
        $metadata = $job->getMetadata();
        $queueItem->setQueueItemId($metadata['id']);
        $this->queue->delete($queueItem);
    }

    /**
     * {@inheritDoc}
     */
    public function fail(JobInterface $job)
    {
        $queueItem = new QueueItem();
        $metadata = $job->getMetadata();
        $queueItem->setQueueItemId($metadata['id']);
        $this->queue->fail($queueItem);
    }

    /**
     * {@inheritDoc}
     */
    public function complete(JobInterface $job)
    {
        $queueItem = new QueueItem();
        $metadata = $job->getMetadata();
        $queueItem->setQueueItemId($metadata['id']);
        $this->queue->complete($queueItem);
    }

    /**
     * {@inheritDoc}
     */
    public function lock(JobInterface $job)
    {
        $queueItem = new QueueItem();
        $metadata = $job->getMetadata();
        $queueItem->setQueueItemId($metadata['id']);
        $this->queue->lock($queueItem);
    }

}
