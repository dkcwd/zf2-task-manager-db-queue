<?php

namespace Zf2TaskManagerDbQueue\Queue\Service;

use Zf2TaskManagerDbQueue\Queue\DbQueue;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * SqsQueueFactory
 */
class DbQueueFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function createService(ServiceLocatorInterface $serviceLocator, $name = '', $requestedName = '')
    {
        $parentLocator    = $serviceLocator->getServiceLocator();
        $queue = $parentLocator->get('Zf2TaskManagerDbQueue\Service\QueueService');
        $jobPluginManager = $parentLocator->get('SlmQueue\Job\JobPluginManager');

        return new DbQueue($queue, $requestedName, $jobPluginManager);
    }
}
