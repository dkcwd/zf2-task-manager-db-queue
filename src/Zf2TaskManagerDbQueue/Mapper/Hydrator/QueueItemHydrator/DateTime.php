<?php

namespace Zf2TaskManagerDbQueue\Mapper\Hydrator\QueueItemHydrator;

use Zend\Stdlib\Hydrator\Strategy\StrategyInterface;

class DateTime implements StrategyInterface
{
    public function extract($value)
    {
        return $value->format('Y-m-d H:i:s');
    }

    public function hydrate($value)
    {
        return new \DateTime($value);
    }
}