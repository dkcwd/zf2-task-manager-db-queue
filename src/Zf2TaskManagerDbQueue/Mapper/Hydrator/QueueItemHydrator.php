<?php

namespace Zf2TaskManagerDbQueue\Mapper\Hydrator;

use Zf2TaskManagerDbQueue\Mapper\Hydrator\QueueItemHydrator\DateTime;
use Zf2TaskManagerDbQueue\Mapper\Hydrator\QueueItemHydrator\ArgFormat;
use Zend\Stdlib\Hydrator\ClassMethods;

class QueueItemHydrator extends ClassMethods
{
    public function __construct($underscoreSeparatedKeys = true)
    {
        parent::__construct($underscoreSeparatedKeys);
        $this->addStrategy('queue_item_start_date_time', new DateTime());
        $this->addStrategy('queue_item_created_date', new DateTime());
    }
}