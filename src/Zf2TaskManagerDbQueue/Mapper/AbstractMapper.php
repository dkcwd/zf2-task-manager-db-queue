<?php

namespace Zf2TaskManagerDbQueue\Mapper;

use Zend\Db\TableGateway\TableGateway;

abstract class AbstractMapper
{
    /** @var TableGateway */
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
}