<?php

namespace Zf2TaskManagerDbQueue\Mapper\Service;

use Zf2TaskManagerDbQueue\Entity\QueueItem;
use Zf2TaskManagerDbQueue\Mapper\Hydrator\QueueItemHydrator;
use Zf2TaskManagerDbQueue\Mapper\QueueMapper;

use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class QueueMapperFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $db = $serviceLocator->get('Zend\Db\Adapter');

        $set = new HydratingResultSet();
        $set->setObjectPrototype(new QueueItem());
        $set->setHydrator(new QueueItemHydrator());

        $tableGateway = new TableGateway('queue', $db, null, $set);
        $mapper = new QueueMapper($tableGateway);

        return $mapper;
    }
}