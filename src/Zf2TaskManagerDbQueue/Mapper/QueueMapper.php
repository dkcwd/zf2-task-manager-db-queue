<?php

namespace Zf2TaskManagerDbQueue\Mapper;

use Zf2TaskManagerDbQueue\Entity\QueueItem;
use Zf2TaskManagerDbQueue\Mapper\Hydrator\QueueItemHydrator;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Where;

class QueueMapper extends AbstractMapper
{
    /**
     * @param QueueItem $queue
     * @return int
     */
    public function insert(QueueItem $queue)
    {
        $hydrator = new QueueItemHydrator();
        $this->tableGateway->insert(
            $hydrator->extract($queue)
        );
        return $this->tableGateway->getLastInsertValue();
    }

    /**
     * @param QueueItem $queue
     * @return int
     */
    public function fail(QueueItem $queue)
    {
        return $this->tableGateway->update(
            array(
                'queue_item_status' => QueueItem::STATUS_ERROR_WHILE_PROCESSING
            ),
            array('queue_item_id' => $queue->getQueueItemId())
        );
    }

    /**
     * @param QueueItem $queue
     * @return int
     */
    public function lock(QueueItem $queue)
    {
        return $this->tableGateway->update(
            array(
                'queue_item_status' => QueueItem::STATUS_LOCKED_FOR_PROCESSING
            ),
            array('queue_item_id' => $queue->getQueueItemId())
        );
    }

    /**
     * @param QueueItem $queue
     * @return int
     */
    public function complete(QueueItem $queue)
    {
        return $this->tableGateway->update(
            array(
                'queue_item_status' => QueueItem::STATUS_PROCESSING_COMPLETE
            ),
            array('queue_item_id' => $queue->getQueueItemId())
        );
    }

    /**
     * @param array $fields
     * @return QueueItem
     */
    public function getEntityByFields(array $fields)
    {
        $set = $this->tableGateway->select($fields);
        if ($set->count() == 0) {
            return null;
        }
        return $set->current();
    }

    /**
     * return QueueItem|null
     */
    public function getJob()
    {
        $where = new Where();
        $where->in('queue_item_status', array('Ready', 'Error'));
        $set = $this->tableGateway->select($where);
        if ($set->count() == 0) {
            return null;
        }

        return $set->current();
    }
}