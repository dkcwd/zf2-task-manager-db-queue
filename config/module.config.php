<?php

/**
 * Define logging constants for use by the worker
 */
defined('ZF2_TASK_MANAGER_DB_QUEUE_ERROR_LOG')
    || define ('ZF2_TASK_MANAGER_DB_QUEUE_ERROR_LOG', sys_get_temp_dir() . '/zf2-db-queue-error-log.txt');
defined('ZF2_TASK_MANAGER_DB_QUEUE_USE_ALTERNATE_ERROR_LOG')
    || define ('ZF2_TASK_MANAGER_DB_QUEUE_USE_ALTERNATE_ERROR_LOG', true);

return array(
    'controllers' => array(
        'factories' => array(
            'Zf2TaskManagerDbQueue\Controller\DbQueueWorkerController' => 'Zf2TaskManagerDbQueue\Controller\Service\DbQueueWorkerControllerFactory',
        ),
    ),
    'slm_queue' => array(
        'queue_manager' => array(
            'factories' => array(
                'processQueue' => 'Zf2TaskManagerDbQueue\Queue\Service\DbQueueFactory',
            ),
        ),
    ),
    'service_manager' => array(
        'invokables' => array(
        ),
        'factories' => array(
            'Zf2TaskManagerDbQueue\Mapper\QueueMapper' => 'Zf2TaskManagerDbQueue\Mapper\Service\QueueMapperFactory',
            'Zf2TaskManagerDbQueue\Service\QueueService' => 'Zf2TaskManagerDbQueue\Service\Service\QueueServiceFactory',
            'Zf2TaskManagerDbQueue\Worker\DbQueueWorker' => 'SlmQueue\Factory\WorkerFactory'
        ),
    ),
    'console'   => array(
        'router' => array(
            'routes' => array(
                'slm-queue-db-worker' => array(
                    'type'    => 'Simple',
                    'options' => array(
                        'route'    => 'queue db <queue>',
                        'defaults' => array(
                            'controller' => 'Zf2TaskManagerDbQueue\Controller\DbQueueWorkerController',
                            'action'     => 'process'
                        ),
                    ),
                ),
            ),
        ),
    ),
);
